#!/bin/bash
here=$(dirname $0)
#source $here/config.sh
PLOT="python3 myplot.py"
#ntuples=/scratch5/benane/ntuples_new/

branches="B0_M"

trap "kill 0" EXIT
for year in  "2018"; do
	for pol in "down"; do
		for sim in "Sim09k"; do
			data=roottuples/Bd_Dst3pi_11266018_${year}_${pol}_${sim}-ReDecay01only_reweight.root
			MC=roottuples/Bd_Dst3pi_11266018_${year}_${pol}_${sim}-ReDecay01only_reweight.root
			for branch in $branches; do
				$PLOT --file $MC $MC \
				--path "DecayTree" "DecayTree" \
				--branch $branch "B0_MM" \
				--weight "" "" \
				--drawopt "hist" "hist" \
				--output newplots/${branch}_True_Reco_${year}_${pol}.pdf \
				--title "${branch}" \
				--pulls1 --scale --statopt \
				--bins 100 --upper 5400 --lower 5100
			done
		done
	done
done

code newplots/B0_M_True_Reco_2018_down.pdf