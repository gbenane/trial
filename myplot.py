
#!/usr/bin/env python
import argparse
import ROOT
import math
ROOT.gROOT.SetBatch(True)
ROOT.gStyle.SetOptStat(1111)
colours = [
	ROOT.kBlack,
	ROOT.kGreen+1,
	ROOT.kRed+1,
	ROOT.kBlue+1,
	ROOT.kYellow+1,
	ROOT.kMagenta+2,
	ROOT.kCyan+1,
	ROOT.kOrange+1,
	ROOT.kSpring+3,
	ROOT.kTeal+2,
	ROOT.kAzure+1,
	ROOT.kViolet-8,
	ROOT.kPink-4,
]


def styleaxis(axis):
        axis.SetTitleFont(132)
        axis.SetLabelFont(132)
        axis.SetNdivisions(505)
        axis.SetTitleOffset(1.10)
        axis.SetTitleSize(0.065)
        axis.SetLabelSize(0.055)
        axis.SetLabelOffset(0.015)

def manual_pull_hist(exphist, obshist):
        ROOT.gStyle.SetOptStat(0)
        pullhist = ROOT.TH1D(obshist)
        success = pullhist.Add(exphist, -1)
        if not success:
                raise RuntimeError("Incompatibile histograms used to make a pull distribution")
        for i in range(1, pullhist.GetNbinsX()+1):
                residual = pullhist.GetBinContent(i)
                sigma = math.sqrt(exphist.GetBinError(i)**2 + obshist.GetBinError(i)**2)
                if sigma == 0:
                        continue
                pullhist.SetBinContent(i, residual/sigma)
        ymax = max([abs(pullhist.GetMaximum()), abs(pullhist.GetMinimum()), 6])
        pullhist.SetMaximum(ymax)
        pullhist.SetMinimum(-ymax)
        #pullhist.GetXaxis().SetTitle(exphist.GetXaxis().GetTitle())
        return pullhist

def stylepullaxes(xaxis, yaxis):
        """
                Style the axes of the pull plot
        """
        styleaxis(xaxis)
        styleaxis(yaxis)
        yaxis.SetTitle("Pull")
        yaxis.CenterTitle()
        #xaxis.SetTitleOffset(1.20)
        yaxis.SetTitleOffset(0.40)
        #xaxis.SetTitleSize(0.17)
        yaxis.SetTitleSize(0.17)
        xaxis.SetLabelSize(0.15)
        yaxis.SetLabelSize(0.10)
        xaxis.SetTickLength(0.1)
        yaxis.SetNdivisions(5)

def main(args):
	plot = ROOT.TCanvas("plot","",200,200)
	ROOT.gStyle.SetOptStat(1)
	hists = []
	files = [ROOT.TFile.Open(fname) for fname in args.file]
	if args.auto:
		intree = files[0].Get(args.path[0])
		upper = intree.GetMaximum(args.branch[0])
		lower = intree.GetMinimum(args.branch[0])
	else:
		upper = args.upper
		lower = args.lower
	if args.drawopt != None and len(args.drawopt) == len(files):
		drawopts = args.drawopt
	else:
		drawopts = ["hist"]*len(files)
	ROOT.gStyle.SetOptStat(1)	
	if args.pulls1:
		mainpad = ROOT.TPad("mainpad1", "", 0.00, 0.3, 1.00, 1.00)
		mainpad.SetMargin(0.15,0.05,0.03,0.05)
		mainpad.SetTicks(1,1)
		mainpad.Draw()
		pullpad = ROOT.TPad("pullpad", "", 0.00, 0.00, 1.00, 0.3)
		pullpad.SetMargin(0.15,0.05,0.50,0.02)
		pullpad.SetTicks(1,1)
		pullpad.Draw()
		mainpad.cd()
	else:
		mainpad1 = ROOT.TPad("mainpad1", "", 0.00, 0.00, 1.00, 1.00)
		mainpad1.SetMargin(0.15,0.05,0.2,0.05)
		mainpad1.SetTicks(1,1)
		mainpad1.Draw()
		mainpad1.cd()
	for i,(infile, path, branch, weight, drawopt) in enumerate(zip(files, args.path, args.branch, args.weight, drawopts)):
		intree = infile.Get(path)
		histname = "hist"+str(i)
		hists.append(ROOT.TH1D(histname, "", args.bins, lower, upper))
		if i > 0:
			drawopt += "same"
		elif args.title != None:
			hists[-1].GetXaxis().SetTitle(args.title)
		if args.cut != None:
			if weight != "":
				weight = "(" + weight + ")*(" + args.cut + ")"
			else:
				weight = args.cut
		intree.Draw(branch+">>"+histname, weight, drawopt)
		hists[-1].SetLineColor(colours[i])
		hists[-1].SetMarkerColor(colours[i])
		hists[-1].SetLineWidth(2)
		hists[-1].SetMarkerSize(0.3)
		hists[-1].SetMarkerStyle(9)
		if args.scale:
			hists[-1].Scale(1.0/hists[-1].Integral())
		else:
			pass
	hists[0].SetMaximum(max([hist.GetMaximum()*1.1 for hist in hists]))
	Xmax = (max([hist.GetMaximum() for hist in hists]))
	if args.statopt:
		for i in range(len(files)):
			mean = round(hists[i].GetMean(),3)
			entry = round(hists[i].GetEntries())
			std = round(hists[i].GetStdDev(),3)
			t = ROOT.TLatex()
			t.SetTextColor( colours[i] )
			t.SetTextSize( 0.04 )
			t.SetTextAlign( 12 )
			t.SetTextFont(0)
			if i==0:
				t.SetTitle("Data")
			else:
				t.SetTitle("MC")
			x = 0.04
			t.DrawLatex( upper-100, Xmax*(1-x*1)-i/55, 'Variable {}'.format(args.branch[i]) )
			t.DrawLatex( upper-100, Xmax*(1-x*2)-i/55, 'Entries ={}'.format(entry))
			t.DrawLatex( upper-100, Xmax*(1-x*3)-i/55, 'Mean ={}'.format(mean))
			t.DrawLatex( upper-100, Xmax*(1-x*4)-i/55, 'StdDev ={}'.format(std))
	if args.pulls1:
		pullpad.cd()
		pullhist = manual_pull_hist(hists[0], hists[1])
		#ROOT.gStyle.SetOptStat(0)
		pullhist.Draw("hist B")
		pullhist.SetFillColor(ROOT.kGreen+1)
		pullhist.SetLineWidth(0)
		stylepullaxes(pullhist.GetXaxis(), pullhist.GetYaxis())
	#plot.Draw()
	#plot.SetLogy(args.logy)
	plot.SaveAs(args.output)

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description = "Superimpose a list of distributions with different weights onto the same plot")
	parser.add_argument("--file", nargs="+", help="ntuples to read")
	parser.add_argument("--branch", nargs="+", help="branches to plot")
	parser.add_argument("--weight", nargs="+", help="weight variables/formulae associated with each plotted branch")
	parser.add_argument("--path", nargs="+", help="path to ntuple")
	parser.add_argument("--output", default="plot.pdf", help="output file")
	parser.add_argument("--bins", default=50, type=int, help="number of bins")
	parser.add_argument("--upper", type=float, help="upper x-axis limit")
	parser.add_argument("--lower", type=float, help="lower x-axis limit")
	parser.add_argument("--auto", action="store_true", help="automatic axis ranges using first branch, overrides --upper and --lower")
	parser.add_argument("--logy", action="store_true", help="log scale y-axis")
	parser.add_argument("--pulls1", action="store_true", help="draw pull plot between 1st and 2nd plot")
	parser.add_argument("--pulls2", action="store_true", help="draw pull plot between 1st and 3nd plot")
	parser.add_argument("--pulls3", action="store_true", help="draw pull plot between 2st and 3nd plot")
	parser.add_argument("--drawopt", nargs="+", help="draw option")
	parser.add_argument("--title", default=None, help="x-axis title")
	parser.add_argument("--cut", default=None, help="cut string")
	parser.add_argument("--statopt", action="store_true", help="print stat info")
	parser.add_argument("--scale", action="store_true", help="rescale distribution for a better comparison")
	main(parser.parse_args())


